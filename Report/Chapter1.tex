% !TEX root:Report.tex
% \graphicspath{{./../TSP/results/}}
\chapter{Solving the TSP problem with an Evolutionary Algorithm}

\section{Run the Python Script}
Go in the folder \verb+TSP/+ and run in a shell the command: 
\begin{center}
    \verb+$ python3 main.py [file to use] [method] [number of generations]+
\end{center}
The default arguments are \verb+./map/ei8246.tsp hillclimbing 5+ which
will run 5 interations of the hillclimbing
algorithm on the file ei8246.tsp. \verb+[method]+ also accepts \verb+main+ as argument
to run tests using a tournament selection. More details are given in the Appendix.

\section{Evolutionary Algorithm to solve the TSP}
\subsection{Hillclimbing}
Running $n$ times the hillclimbing algorithm with a population of one individual,
as long as the starting point is different each time, is not different from running
once the algorithm with a population of $n$ individuals with $n$ different starting
points.
\\
\begin{table}[h]
    \centering
    \begin{tabular}{|c|c|c|}
        \hline
        Number of Iterations & Number Of Iterations without Update
        & Distance ($10^9 m$)\\
        \hline
       1000/1000 & 1/100 & 76.94 \\
       1000/1000 & 1/100 & 78.03 \\
       1000/1000 & 0/100 & 77.16 \\
       1000/1000 & 0/100 & 78.08 \\
       1000/1000 & 6/100 & 77.8  \\
       \hline
    \end{tabular}\\
    \caption[Example of tests run on the file ei8246.tsp.]{Exemple of tests run on the file ei8246.tsp. Average time per result: 675 sec}
    \label{tabei8246}
\end{table}

\begin{table}[h]
    \centering
    \begin{tabular}{|c|c|c|}
        \hline
        Number of Iterations & Number Of Iterations without Update
        & Distance ($10^6 m$)\\
        \hline
       206/1000 & 100/100 & 3.27 \\
       302/1000 & 100/100 & 4.62 \\
       384/1000 & 100/100 & 3.27 \\
       463/1000 & 100/100 & 3.94 \\
       170/1000 & 100/100 & 4.16  \\
       \hline
    \end{tabular}\\
    \caption[Example of tests run on the file ei15.tsp.]{Exemple of tests run on the file ei15.tsp. Average time per result: 4 sec}
    \label{tabei15}
\end{table}

Tests have been run on a smaller file (15 cities, \cref{tabei15}) in order to avoid 
having a long processing time and to check the influence of the maximum 
number of iterations without any update. While on the full file (\cref{tabei8246}),
the stop condition was the number of iterations, it was the 
number without update during the tests on the small file (\cref{tabei15}).
It seems to be a correlation between the size of the individuals and this parameter.

The random aspect of the EA may be the reason why: for bigger individuals, this 
parameter only influences the result if the number of iterations is big enough 
($>30 *$ size of the file). When the algorithm stops because of this parameter, 
the result is likely to be a local minimum.

\subsection{Selection with Tournament}
During these tests, the Tournament was the selection method of individuals 
among the population. The following parameters have been changed in the algorithm:
\begin{itemize}
    \item The size of the population
    \item The size of the tournament (\textit{ie. the pressure of the selection})
    \item The maximum number of iterations
    \item The maximum number of iterations without any update of the population
\end{itemize}

\begin{table}[h]
    \centering
    \captionsetup{justification=centering}
    \begin{tabular}{|c|c|c|c|}
        \hline
        Size of Population & Size of Tournament & Avg \& Min Distance ($10^9 m$) & Avg Elapsed Time  \\ & & & per individual (sec)\\
        \hline
        12 & 3 & Avg: 78.633 | Min: 78.420 & 2900 \\
        12 & 6 & Avg: 78.118 | Min: 77.875 & 3500 \\
        12 & 9 & Avg: 77.561 | Min: 76.687 & 4000 \\
        \hline
    \end{tabular}
    \caption[Average Results based on the Tournament Selection with the file ei8246.tsp]
        {Results based on the tournament selection, based on the file ei8246.tsp.\\
        Maximum Number of Iterations: 1000, Maximum Number of Iterations without Update: 100}
    \label{table:tournamentei8246}
\end{table}

\begin{table}[h]
    \centering
    \captionsetup{justification=centering}
    \begin{tabular}{|c|c|c|c|}
        \hline
        Size of Population & Size of Tournament & Avg \& Min Distance ($10^6 m$) & Avg Elapsed Time  \\ & & & per individual (sec)\\
        \hline
        12 & 3 & Avg: 7.848 | Min: 7.643 & 360 \\
        12 & 6 & Avg: 7.442 | Min: 7.215 & 420 \\
        12 & 9 & Avg: 7.415 | Min: 7.254 & 468 \\
        \hline
    \end{tabular}
    \caption[Average Results based on the Tournament Selection with the file e1001.tsp]
        {Results based on the tournament selection, based on the file ei1001.tsp.\\
        Maximum Number of Iterations: 1000, Maximum Number of Iterations without Update: 100}
    \label{table:tournamentei1001}
\end{table}

\begin{table}[h]
    \centering
    \captionsetup{justification=centering}
    \begin{tabular}{|c|c|c|c|}
        \hline
        Size of Population & Size of Tournament & Avg \& Min Distance ($10^6 m$) & Avg Elapsed Time  \\ & & & per individual (sec)\\
        \hline
        12 & 3 & Avg: 3.245 | Min: 3.212 & 3.9 \\
        12 & 6 & Avg: 3.249 | Min: 3.212 & 4.5 \\
        12 & 9 & Avg: 3.250 | Min: 3.212 & 5.4 \\
        \hline
    \end{tabular}
    \caption[Average Results based on the Tournament Selection with the file ei15.tsp]
        {Results based on the tournament selection, based on the file ei15.tsp.\\
        Maximum Number of Iterations: 1000, Maximum Number of Iterations without 
        Update: 100}
    \label{table:tournamentei15}
\end{table}

\textit{\textbf{NB:} These results come from five different tests in a row with the
same parameters. To have more reliable results, more tests would be required.}


According to the result shown in the tables \ref{table:tournamentei8246},
\ref{table:tournamentei1001} and \ref{table:tournamentei15}, releasing the 
pressure of the selection seems to make the computation much longer without 
giving any advantages on the result.
Nonetheless, having a pressure too high seems to prevent the algorithm to work
at its best, by adding another layer of unpredictability.

\textit{The parameters of the maximum number of iterations have also been changed in the other
    tests. However, on smaller file, increasing these numbers did not necessarily
    provide a better result, while worsering the execution time.}

\subsection{Conclusions}
From the results shown above, the hillclimbing method seems to give similar
results as the tournament selection method in solving this trade salesman problem.
%Considering the elapsed time may not be the primary concern.
However, when an
algorithm requires several hours to find a solution which is not necessarily the best
one due to the random aspect of the generation, the hillclimbing method is more
interesting because having a population of one individual speeds up the computation.
