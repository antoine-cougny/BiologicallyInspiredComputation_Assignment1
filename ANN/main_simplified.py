#! /usr/bin/env python3
# coding:utf-8

import numpy as np
import matplotlib.pyplot as plt
from ANN import *

def main(annLayers,
         functionToStudy,
         activationFunction,
         scaleOrNorm=False,
         minInit=-5,
         maxInit=5,
         numPt=200, 
         numIter=80001
        ):

    # Set of data
    x = np.linspace(-5, 5, numPt).reshape((numPt,1))
    y = functionToStudy(x)
    
    inputLayer = annLayers[0]
    hiddenLayer = annLayers[1]
    outputLayer = annLayers[2]

    ## Normalization
    if scaleOrNorm:
        x_mean = np.mean(x)
        x_std = np.std(x)
        x_norm = (x - x_mean)/x_std

        y_mean = np.mean(y)
        y_std = np.std(y)
        y_norm = (y - y_mean)/y_std
        
        y_delta = y_std
        y_diff = y_mean

    else:
        x_min = np.min(x)
        x_max = np.max(x)
        x_norm = (x - x_min)/(x_max - x_min)

        y_min = np.min(y)
        y_max = np.max(y)
        y_delta = (y_max - y_min)
        y_diff = x_min

        y_norm = (y - y_min)/y_delta

    ############################################################################
    ## Variables used for the coursework
    listError = 10*[np.inf]
    minError = np.inf
    arrayOfWeightMinError = np.array(1)

    for k in range(10):
        ## Create a new object
        ann = ANN(inputLayer, hiddenLayer, outputLayer, activationFunction)
        ann.initANN(minInit, maxInit, seed=np.random.randint(100))

        ## Compute error on last layer
        yN = ann.useANN(x_norm, False)
        error = y_norm - yN 

        print("Error at the test number %i: %f"%(k,np.sum(np.abs(error))))

        listError[k] = np.sum(error)
        if listError[k] < minError:
            minError = listError[k]
            arrayOfWeightMinError = ann.arrayOfWeights

    print("\n\n\nArrayOfWeight giving the minimum error on the 10 tests")
    print(arrayOfWeightMinError[0],'\n',arrayOfWeightMinError[1].flatten())
    ann.arrayOfWeights = arrayOfWeightMinError
    yN = ann.useANN(x_norm, False)

    ############################################################################
    plt.figure()
    plt.plot(x_norm, y_norm, 'r', label="f(x)")
    plt.plot(x_norm, yN, 'b', label="Approximation with MLP")
    plt.legend(loc='best')
    plt.show()

if __name__ == '__main__':

    # If scaleOrNorm is True, data will be normalized (using mean and std)
    # If it is False, data will be scaled to [0; 1]*[0; 1]. It is needed for some
    # activation functions like the sigmoid.
    main([1, [5], 1],
         cubic,
         sigmoid,
         scaleOrNorm=False,
         numPt=10
        )
 
