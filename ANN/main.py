#! /usr/bin/env python3
# coding:utf-8

import numpy as np
import matplotlib.pyplot as plt
from ANN import *

def main(annLayers,
         functionToStudy,
         activationFunction,
         scaleOrNorm=False,
         minInit=-5,
         maxInit=5,
         numPt=200, 
         numIter=80001
        ):

    # Set of data
    x = np.linspace(-5, 5, numPt).reshape((numPt,1))
    y = functionToStudy(x)
    
    inputLayer = annLayers[0]
    hiddenLayer = annLayers[1]
    outputLayer = annLayers[2]

    ## Normalization
    if scaleOrNorm:
        x_mean = np.mean(x)
        x_std = np.std(x)
        x_norm = (x - x_mean)/x_std

        y_mean = np.mean(y)
        y_std = np.std(y)
        y_norm = (y - y_mean)/y_std
        
        y_delta = y_std
        y_diff = y_mean

    else:
        x_min = np.min(x)
        x_max = np.max(x)
        x_norm = (x - x_min)/(x_max - x_min)

        y_min = np.min(y)
        y_max = np.max(y)
        y_delta = (y_max - y_min)
        y_diff = x_min

        y_norm = (y - y_min)/y_delta

    ## On Batch Training
    ann = ANN(inputLayer, hiddenLayer, outputLayer, activationFunction)
    ann.initANN(minInit, maxInit)
    ann.runTraining(x_norm, y_norm, numIter)
    yN = ann.useANN(x_norm, False)

    ## Online Training
    ann2 = ANN(inputLayer, hiddenLayer, outputLayer, activationFunction)
    ann2.initANN(minInit, maxInit)
    ann2.runOnlineTraining(x_norm, y_norm, numIter)
    yN2 = ann2.useANN(x_norm, False)


    plt.figure()
    plt.plot(x_norm, y_norm, 'r', label="f(x)")
    plt.plot(x_norm, yN, 'b', label="Approximation with MLP (Batch)")
    plt.plot(x_norm, yN2, 'g', label="Online Training")
    # plt.plot(x, y, 'r', label="f(x)")
    # plt.plot(x, yN*y_delta-np.abs(y_diff), 'b', label="Approximation with MLP (Batch)")
    # plt.plot(x, yN2*y_delta-np.abs(y_diff), 'g', label="Online Training")
    plt.legend(loc='best')
    plt.show()

if __name__ == '__main__':
    print("Initializing")
    # print(sys.argv)

    # import optparse
    # parser = optparse.OptionParser()
    # parser.add_option('-f', '--function',
    #     action="store", dest="function",
    #     help="query string", default="spam")
    # parser.add_option('-m', '--method',
    #     action="store", dest="method",
    #     help="query string", default="spam")
    # parser.add_option('-n', '--numIter',
    #     action="store", dest="numIter",
    #     help="num intquery string", default="spam")
    # options, args = parser.parse_args()
    # print( 'Query string:', options.method, options.function, options.numIter)



    # If scaleOrNorm is True, data will be normalized (using mean and std)
    # If it is False, data will be scaled to [0; 1]*[0; 1]. It is needed for some
    # activation functions like the sigmoid.
    main([1, [5], 1],
         cubic,
         sigmoid,
         scaleOrNorm=False
        )
