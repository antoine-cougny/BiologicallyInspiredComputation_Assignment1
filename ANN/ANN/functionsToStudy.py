#! /usr/bin/python3
# coding:utf-8

from numpy import sin, tanh, cos, exp

################################################################################
##########
##########                  Functions to Study
##########
################################################################################

def linear(x):
    return(x)

def cubic(x):
    return(x**3)

def sine(x):
    return(sin(x))

# def tanh(x):
#    return (np.tanh(x))

def xor(x1, x2):
    if (x1 and x2) or (not(x1) and not(x2)):
        return False
    else:
        return True

def complex(a, b):
    return(1.9*(1.35 + exp(a + b)*sin(13*(a - 0.6)**2)*sin(7*b**2)))

if __name__ == '__main__':
    print(sine(3.14/2))
