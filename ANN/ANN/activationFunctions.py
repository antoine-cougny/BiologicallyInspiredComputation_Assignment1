#! /usr/bin/env python3
# coding:utf-8

from numpy import sin, tanh, cos, exp

################################################################################
##########
##########                  Activation Functions
##########
################################################################################

def null(x, deriv=False):
    if deriv:
        return (0)
    return(0)

def sigmoid(x, deriv=False):
    if deriv:
        return sigmoid(x) * (1-sigmoid(x))
    return (1./(1+exp(-x)))

def hyperbolicTangent(x, deriv=False):
    if deriv:
        return (1 - hyperbolicTangent(x)**2)
    return (tanh(x))

def cosine(x, deriv=False):
    if deriv:
        return (-sin(x))
    return(cos(x))

def gaussian(x, deriv=False):
    if deriv:
        return (-x*gaussian(x))
    return (exp(-0.5*x**2))
