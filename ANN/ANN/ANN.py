#! /usr/bin/env python3
# coding:utf-8

import numpy as np
from .activationFunctions import *


"""
Create an ANN and train it with a set of data
"""

class ANN():
    def __init__(self, sizeInput, hiddenLayers,
                 sizeOutput, function, learningRate=0.25
                ):
        """
        Arguments:
            sizeInput: int - number of neurons for the input layer

            hiddenLayers: numpy Array(n) - each line of this array is an
                hidden layer with the number of neurons/
                Exemple : Array([5, 4, 5]) gives three hidden layers with
                respectively 5, 4 and 5 neurons each.

            sizeOutput: int - number of neurons for the output layer

            seed: int - seed for the random generation

        Return:
            An ANN Object
        """
        self.sizeInput      = sizeInput
        self.hiddenLayers   = hiddenLayers
        self.sizeOutput     = sizeOutput
        self.activationFct  = function
        self.learningRate   = learningRate

        # Memory allocation with Python Lists
        self.arrayOfWeights = [0]*(len(hiddenLayers) + 1)
        self.feedLayers     = [0]*(len(hiddenLayers) + 2)
        self.arrayOfDeltas  = [0]*(len(hiddenLayers) + 1)
        self.weightOfBias   = [0]*(len(hiddenLayers) + 1)
        self.arrayOfResults = [0]*(len(hiddenLayers) + 2)

        self.error = None # Used to get the error on the final layer 

    def initANN(self, minValue, maxValue, seed=1):
        """
        Initialize the matrix of weights to link the different neurons layers
        """
        np.random.seed(seed)
        # mean = (minValue + maxValue)/2
        maxRnd = abs(minValue) + abs(maxValue)
        # print(mean, maxRnd)
        for i in range(len(self.hiddenLayers) + 1):
            # Random values are element of [minValue, maxValue]
            self.arrayOfWeights[i] = maxRnd*np.random.random((self.indexPrev(i),
                                                              self.indexNext(i)
                                                             )) - maxRnd/2
            self.weightOfBias[i] = maxRnd*np.random.random((1,
                                                            self.indexNext(i)
                                                           )) - maxRnd/2

    def indexPrev(self, i):
        """
        Functions used to generate the size of the matrix of wheights on the left
        side
        """
        if i == 0:
            # First layer
            return self.sizeInput
        return self.hiddenLayers[i-1]

    def indexNext(self, i):
        """
        Functions used to generate the size of the matrix of wheights on the
        right side
        """
        if i == len(self.hiddenLayers):
            # Last layer
            return self.sizeOutput
        return self.hiddenLayers[i]

    def feedForwardNeurons(self, inputFeed):
        """
        First Step of using an ANN is to feed the neurons layers.
        This is a parallel feeding.

        Input:
            inputFeed: Numpy Array of dim (n, sizeInput).
        """

        if len(inputFeed[0]) < self.sizeInput:
            print("\n\tError!")
            print("\t------")
            print("\tSize Given: ", len(inputFeed[:]))
            print("\tSize of ANN: ", self.sizeInput)
            print("\tThe given input is smaller than the first layer of neurons")
            print("\t----------------------------------------------------------\n")
            return False
        elif len(inputFeed[0]) > self.sizeInput:
            print("\n\tError!")
            print("\t------")
            print("\tSize Given: ", len(inputFeed[:]))
            print("\tSize of ANN: ", self.sizeInput)
            print("\tThe given input is bigger than the first layer of neurons")
            print("\t---------------------------------------------------------\n")
            return False

        self.feedLayers[0] = inputFeed
        # Each loop is a layer of neurons
        for i in range(1, len(self.feedLayers)):
            # Result before the activation function. Used in back propagation
            self.arrayOfResults[i] = self.feedLayers[i-1] @ self.arrayOfWeights[i-1] \
                                     + self.weightOfBias[i-1]
            # Complete the forward feeding by using the activation function
            self.feedLayers[i] = self.activationFct(self.arrayOfResults[i])
        return True

    def calculateError(self, outputSet):
        """
        Calculate the error on the different layers (backward propagation)
        """
        # First step: Compute error on last layer
        error = outputSet - self.feedLayers[-1]
        self.error = error
        delta = error*self.activationFct(self.arrayOfResults[-1], deriv=True)
        self.arrayOfDeltas[-1] = delta

        # Next Step: Compute error backward
        for i in range(len(self.arrayOfDeltas)-2, -1, -1):
            error = self.arrayOfDeltas[i+1] @ self.arrayOfWeights[i+1].T
            delta = error * self.activationFct(self.arrayOfResults[i+1], True)
            self.arrayOfDeltas[i] = delta

    def updateWeight(self):
        """
        Update the weight matrix with the error computed before
        """
        for i in range(0, len(self.arrayOfWeights)):
            update = self.learningRate \
                    * self.feedLayers[i].T @ self.arrayOfDeltas[i]
            self.arrayOfWeights[i] += update
            self.weightOfBias[i] += self.learningRate \
                                    * np.mean(self.arrayOfDeltas[i], axis=0)

    def printError(self, counter, modulo=10**4):
        """
        Print the mean of the error on the final layer
        """
        if (counter % modulo) == 0:
            print(" ("+str(counter)+")\t Error:" + str(np.mean(np.abs(self.error))))

    def runTraining(self, inputSet, outputSet,
                    numIter=10**5+1, modulo=10**4, printError=True
                   ):
        """
        Run the training on the ANN
        """
        print("\nStarting the training of the ANN")
        for k in range(numIter):
            if self.feedForwardNeurons(inputSet):
                self.calculateError(outputSet)
                if printError:
                    self.printError(k, modulo)
                self.updateWeight()
            else:
                return

    def runOnlineTraining(self, inputSet, outputSet,
                          numIter=10**5+1, modulo=10**4, printError=True
                         ):
        """
        Run an online training on the ANN
        """
        print("\nStarting the online training of the ANN")

        n = len(inputSet)
        for k in range (numIter):
            # Selection of the input among the possible ones
            index  = np.random.choice(n-1)
            input  = np.array(inputSet[index])
            output = np.array(outputSet[index])
            
            # Reshape to have a column vector
            input = input.reshape((1, len(input)))
            output = output.reshape((1, len(output)))

            # Training
            if self.feedForwardNeurons(input):
                self.calculateError(output)
                if printError:
                    self.printError(k, modulo)
                self.updateWeight()
            else:
                return

    def useANN(self, inputSet, printOutput=True):
        """
        Use the ANN on the input set after having trained the neurons.
        """
        # print("\nNow let's use our trained ANN")
        self.feedForwardNeurons(inputSet)
        output = np.mean(self.feedLayers[-1], axis=1).reshape((len(inputSet), 1))

        if printOutput:
            print("Output:\n", output)

        return (output)


if __name__ == '__main__':
    """
    Unit tests
    """
    def sigmoid(x, deriv=False):
        """
        Sigmoid Function
        """
        if deriv:
            return sigmoid(x)*(1-sigmoid(x))

        return 1/(1+np.exp(-x))

    X = np.array([[0, 0, 1],
                  [0, 1, 1],
                  [1, 0, 1],
                  [1, 1, 1]])

    X3 = np.array([[0, 0],
                   [0, 1],
                   [1, 0],
                   [1, 1]])

    Y = np.array([[0],
                  [1],
                  [1],
                  [0]])

    x2 = np.array([[0, 1, 1],
                   [0, 1, 0],
                   [1, 0, 1],
                   [1, 0, 0],
                   [1, 1, 0],
                   [1, 1, 1],
                   [0, 0, 0],
                   [0, 0, 1]])

    ##############################
    ##      BATCH TRAINING      ##
    ##############################
    ann = ANN(3, [3], 1, sigmoid)
    ann.initANN(-5, 5)
    ann.runTraining(X, Y)
    ann.useANN(x2)

    ##############################
    ##      ONLINE TRAINING     ##
    ##############################
    ann2 = ANN(2, [3], 1, sigmoid)
    ann2.initANN(-5, 5)
    ann2.runOnlineTraining(X3, Y)
    ann2.useANN(X3)
