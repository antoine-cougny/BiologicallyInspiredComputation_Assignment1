# Analyze of Results

- Increasing the number of iterations does not necessarily give a better result
  but the time required increases.  (pop of 1, full file) / (pop of 4 - select 2, full)
  Probably because of size of file.

- Increasing the number of iterations without update gives better result on smaller
files. I think there is a relation between these two. This parameter may be important
when we end very close to the best solution.

- Releasing the pressure (inc size of tournament) seems to make the computation much
longer without a clear advantage on the result


- everything depends on the precision we want to have

- initiative taken: take a shorter file
        influence of parameter "numMaxIterWOUpdate" is important
        12 2 X Y small file: pressure seems to be too important, less interesting result
