Population of 6 individuals

#####################
####     End     ####
#####################
### List too long to be displayed ###

End after 30 interations. 		        	Max: 30
Number of iterations without changes:   0 	Max: 3
Fitness of the Final Individual:        81,170,488,752.00  meters
Elapsed time: 					        52.967491 seconds


 selectionMethod=tournamentSelection,
 evaluateFitnessMethod=computeTripCost,
 mutateMethod=swapTwoElements,
 updateMethod=replaceWorst,
 sizeOfPop=6,
 sizeOfSelection=3,
 typeOfComparaison='low',
 numberOfCities=None,
 maxNbIter=30,
 maxNbIterWithoutChange=3
