

##############################################################

Opening the following file:  ./map/ei15.tsp
#####################
####     End     ####
#####################

 The node in this list are:
	1 - City number 15 located at (51495.5556, 9315.0)
	2 - City number 14 located at (51493.8889, 9709.1667)
	3 - City number 9 located at (51483.3333, 9350.0)
	4 - City number 8 located at (51481.9444, 9762.2222)
	5 - City number 7 located at (51472.7778, 9768.8889)
	6 - City number 10 located at (51483.3333, 9366.6667)
	7 - City number 4 located at (51466.6667, 9716.6667)
	8 - City number 6 located at (51470.0, 9416.3889)
	9 - City number 3 located at (51462.2222, 9433.6111)
	10 - City number 5 located at (51468.6111, 9810.0)
	11 - City number 1 located at (51445.0, 9479.4444)
	12 - City number 2 located at (51455.2778, 9815.8333)
	13 - City number 12 located at (51486.3889, 9724.7222)
	14 - City number 11 located at (51483.6111, 9783.0556)
	15 - City number 13 located at (51487.7778, 9348.0556)

Size of population:			3
Size of Tournament Selection:		2
Length of an individual:		15
Number of iterations: 			90 	Max: 90
Number of iterations without updates: 	5 	Max: 10
Fitness of the Final Individual:         5,086,647.00  meters
Elapsed time: 				0.186824 seconds


##############################################################

Opening the following file:  ./map/ei15.tsp
#####################
####     End     ####
#####################

 The node in this list are:
	1 - City number 3 located at (51462.2222, 9433.6111)
	2 - City number 12 located at (51486.3889, 9724.7222)
	3 - City number 11 located at (51483.6111, 9783.0556)
	4 - City number 10 located at (51483.3333, 9366.6667)
	5 - City number 6 located at (51470.0, 9416.3889)
	6 - City number 7 located at (51472.7778, 9768.8889)
	7 - City number 4 located at (51466.6667, 9716.6667)
	8 - City number 5 located at (51468.6111, 9810.0)
	9 - City number 8 located at (51481.9444, 9762.2222)
	10 - City number 15 located at (51495.5556, 9315.0)
	11 - City number 14 located at (51493.8889, 9709.1667)
	12 - City number 13 located at (51487.7778, 9348.0556)
	13 - City number 9 located at (51483.3333, 9350.0)
	14 - City number 2 located at (51455.2778, 9815.8333)
	15 - City number 1 located at (51445.0, 9479.4444)

Size of population:			3
Size of Tournament Selection:		2
Length of an individual:		15
Number of iterations: 			59 	Max: 90
Number of iterations without updates: 	10 	Max: 10
Fitness of the Final Individual:         5,328,349.00  meters
Elapsed time: 				0.120358 seconds


##############################################################

Opening the following file:  ./map/ei15.tsp
#####################
####     End     ####
#####################

 The node in this list are:
	1 - City number 9 located at (51483.3333, 9350.0)
	2 - City number 10 located at (51483.3333, 9366.6667)
	3 - City number 11 located at (51483.6111, 9783.0556)
	4 - City number 1 located at (51445.0, 9479.4444)
	5 - City number 2 located at (51455.2778, 9815.8333)
	6 - City number 3 located at (51462.2222, 9433.6111)
	7 - City number 15 located at (51495.5556, 9315.0)
	8 - City number 14 located at (51493.8889, 9709.1667)
	9 - City number 12 located at (51486.3889, 9724.7222)
	10 - City number 13 located at (51487.7778, 9348.0556)
	11 - City number 5 located at (51468.6111, 9810.0)
	12 - City number 4 located at (51466.6667, 9716.6667)
	13 - City number 6 located at (51470.0, 9416.3889)
	14 - City number 7 located at (51472.7778, 9768.8889)
	15 - City number 8 located at (51481.9444, 9762.2222)

Size of population:			3
Size of Tournament Selection:		2
Length of an individual:		15
Number of iterations: 			90 	Max: 90
Number of iterations without updates: 	1 	Max: 10
Fitness of the Final Individual:         5,062,511.00  meters
Elapsed time: 				0.204409 seconds


##############################################################

Opening the following file:  ./map/ei15.tsp
#####################
####     End     ####
#####################

 The node in this list are:
	1 - City number 8 located at (51481.9444, 9762.2222)
	2 - City number 7 located at (51472.7778, 9768.8889)
	3 - City number 4 located at (51466.6667, 9716.6667)
	4 - City number 3 located at (51462.2222, 9433.6111)
	5 - City number 2 located at (51455.2778, 9815.8333)
	6 - City number 1 located at (51445.0, 9479.4444)
	7 - City number 5 located at (51468.6111, 9810.0)
	8 - City number 6 located at (51470.0, 9416.3889)
	9 - City number 13 located at (51487.7778, 9348.0556)
	10 - City number 12 located at (51486.3889, 9724.7222)
	11 - City number 11 located at (51483.6111, 9783.0556)
	12 - City number 14 located at (51493.8889, 9709.1667)
	13 - City number 15 located at (51495.5556, 9315.0)
	14 - City number 10 located at (51483.3333, 9366.6667)
	15 - City number 9 located at (51483.3333, 9350.0)

Size of population:			3
Size of Tournament Selection:		2
Length of an individual:		15
Number of iterations: 			78 	Max: 300
Number of iterations without updates: 	10 	Max: 10
Fitness of the Final Individual:         3,752,495.00  meters
Elapsed time: 				0.169871 seconds


##############################################################

Opening the following file:  ./map/ei15.tsp
#####################
####     End     ####
#####################

 The node in this list are:
	1 - City number 14 located at (51493.8889, 9709.1667)
	2 - City number 8 located at (51481.9444, 9762.2222)
	3 - City number 7 located at (51472.7778, 9768.8889)
	4 - City number 11 located at (51483.6111, 9783.0556)
	5 - City number 2 located at (51455.2778, 9815.8333)
	6 - City number 1 located at (51445.0, 9479.4444)
	7 - City number 4 located at (51466.6667, 9716.6667)
	8 - City number 3 located at (51462.2222, 9433.6111)
	9 - City number 5 located at (51468.6111, 9810.0)
	10 - City number 6 located at (51470.0, 9416.3889)
	11 - City number 15 located at (51495.5556, 9315.0)
	12 - City number 12 located at (51486.3889, 9724.7222)
	13 - City number 10 located at (51483.3333, 9366.6667)
	14 - City number 9 located at (51483.3333, 9350.0)
	15 - City number 13 located at (51487.7778, 9348.0556)

Size of population:			3
Size of Tournament Selection:		2
Length of an individual:		15
Number of iterations: 			53 	Max: 300
Number of iterations without updates: 	10 	Max: 10
Fitness of the Final Individual:         5,604,910.00  meters
Elapsed time: 				0.114762 seconds


##############################################################

Opening the following file:  ./map/ei15.tsp
#####################
####     End     ####
#####################

 The node in this list are:
	1 - City number 13 located at (51487.7778, 9348.0556)
	2 - City number 12 located at (51486.3889, 9724.7222)
	3 - City number 14 located at (51493.8889, 9709.1667)
	4 - City number 15 located at (51495.5556, 9315.0)
	5 - City number 10 located at (51483.3333, 9366.6667)
	6 - City number 9 located at (51483.3333, 9350.0)
	7 - City number 11 located at (51483.6111, 9783.0556)
	8 - City number 7 located at (51472.7778, 9768.8889)
	9 - City number 4 located at (51466.6667, 9716.6667)
	10 - City number 2 located at (51455.2778, 9815.8333)
	11 - City number 5 located at (51468.6111, 9810.0)
	12 - City number 6 located at (51470.0, 9416.3889)
	13 - City number 8 located at (51481.9444, 9762.2222)
	14 - City number 1 located at (51445.0, 9479.4444)
	15 - City number 3 located at (51462.2222, 9433.6111)

Size of population:			3
Size of Tournament Selection:		2
Length of an individual:		15
Number of iterations: 			78 	Max: 300
Number of iterations without updates: 	10 	Max: 10
Fitness of the Final Individual:         5,318,875.00  meters
Elapsed time: 				0.174088 seconds
