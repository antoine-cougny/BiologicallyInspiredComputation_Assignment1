

##############################################################

Opening the following file:  ./map/ei15.tsp
#####################
####     End     ####
#####################

 The node in this list are:
	1 - City number 6 located at (51470.0, 9416.3889)
	2 - City number 5 located at (51468.6111, 9810.0)
	3 - City number 4 located at (51466.6667, 9716.6667)
	4 - City number 2 located at (51455.2778, 9815.8333)
	5 - City number 1 located at (51445.0, 9479.4444)
	6 - City number 3 located at (51462.2222, 9433.6111)
	7 - City number 14 located at (51493.8889, 9709.1667)
	8 - City number 15 located at (51495.5556, 9315.0)
	9 - City number 13 located at (51487.7778, 9348.0556)
	10 - City number 12 located at (51486.3889, 9724.7222)
	11 - City number 11 located at (51483.6111, 9783.0556)
	12 - City number 9 located at (51483.3333, 9350.0)
	13 - City number 10 located at (51483.3333, 9366.6667)
	14 - City number 8 located at (51481.9444, 9762.2222)
	15 - City number 7 located at (51472.7778, 9768.8889)

Size of population:			3
Size of Tournament Selection:		2
Length of an individual:		15
Number of iterations: 			300 	Max: 300
Number of iterations without updates: 	4 	Max: 40
Fitness of the Final Individual:         3,481,398.00  meters
Elapsed time: 				0.652891 seconds


##############################################################

Opening the following file:  ./map/ei15.tsp
#####################
####     End     ####
#####################

 The node in this list are:
	1 - City number 8 located at (51481.9444, 9762.2222)
	2 - City number 9 located at (51483.3333, 9350.0)
	3 - City number 12 located at (51486.3889, 9724.7222)
	4 - City number 15 located at (51495.5556, 9315.0)
	5 - City number 14 located at (51493.8889, 9709.1667)
	6 - City number 13 located at (51487.7778, 9348.0556)
	7 - City number 1 located at (51445.0, 9479.4444)
	8 - City number 2 located at (51455.2778, 9815.8333)
	9 - City number 11 located at (51483.6111, 9783.0556)
	10 - City number 10 located at (51483.3333, 9366.6667)
	11 - City number 4 located at (51466.6667, 9716.6667)
	12 - City number 3 located at (51462.2222, 9433.6111)
	13 - City number 5 located at (51468.6111, 9810.0)
	14 - City number 6 located at (51470.0, 9416.3889)
	15 - City number 7 located at (51472.7778, 9768.8889)

Size of population:			3
Size of Tournament Selection:		2
Length of an individual:		15
Number of iterations: 			157 	Max: 300
Number of iterations without updates: 	40 	Max: 40
Fitness of the Final Individual:         5,056,631.00  meters
Elapsed time: 				0.333650 seconds


##############################################################

Opening the following file:  ./map/ei15.tsp
#####################
####     End     ####
#####################

 The node in this list are:
	1 - City number 11 located at (51483.6111, 9783.0556)
	2 - City number 8 located at (51481.9444, 9762.2222)
	3 - City number 7 located at (51472.7778, 9768.8889)
	4 - City number 6 located at (51470.0, 9416.3889)
	5 - City number 5 located at (51468.6111, 9810.0)
	6 - City number 1 located at (51445.0, 9479.4444)
	7 - City number 2 located at (51455.2778, 9815.8333)
	8 - City number 3 located at (51462.2222, 9433.6111)
	9 - City number 4 located at (51466.6667, 9716.6667)
	10 - City number 9 located at (51483.3333, 9350.0)
	11 - City number 10 located at (51483.3333, 9366.6667)
	12 - City number 15 located at (51495.5556, 9315.0)
	13 - City number 14 located at (51493.8889, 9709.1667)
	14 - City number 13 located at (51487.7778, 9348.0556)
	15 - City number 12 located at (51486.3889, 9724.7222)

Size of population:			3
Size of Tournament Selection:		2
Length of an individual:		15
Number of iterations: 			252 	Max: 300
Number of iterations without updates: 	40 	Max: 40
Fitness of the Final Individual:         3,271,696.00  meters
Elapsed time: 				0.552838 seconds
