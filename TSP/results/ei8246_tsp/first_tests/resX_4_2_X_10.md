

##############################################################

Opening the following file:  ./map/ei8246.tsp
#####################
####     End     ####
#####################
### List too long to be displayed ###

Size of population:			4
Size of Tournament Selection:		2
Length of an individual:		8246
Number of iterations: 			30 	Max: 30
Number of iterations without updates: 	0 	Max: 10
Fitness of the Final Individual:    81,753,380,861.00  meters
Elapsed time: 				41.934015 seconds


##############################################################

Opening the following file:  ./map/ei8246.tsp
#####################
####     End     ####
#####################
### List too long to be displayed ###

Size of population:			4
Size of Tournament Selection:		2
Length of an individual:		8246
Number of iterations: 			30 	Max: 30
Number of iterations without updates: 	0 	Max: 10
Fitness of the Final Individual:    81,595,143,865.00  meters
Elapsed time: 				40.375119 seconds


##############################################################

Opening the following file:  ./map/ei8246.tsp
#####################
####     End     ####
#####################
### List too long to be displayed ###

Size of population:			4
Size of Tournament Selection:		2
Length of an individual:		8246
Number of iterations: 			50 	Max: 50
Number of iterations without updates: 	0 	Max: 10
Fitness of the Final Individual:    82,012,888,618.00  meters
Elapsed time: 				66.002170 seconds


##############################################################

Opening the following file:  ./map/ei8246.tsp
#####################
####     End     ####
#####################
### List too long to be displayed ###

Size of population:			4
Size of Tournament Selection:		2
Length of an individual:		8246
Number of iterations: 			50 	Max: 50
Number of iterations without updates: 	0 	Max: 10
Fitness of the Final Individual:    81,733,268,423.00  meters
Elapsed time: 				72.382252 seconds


##############################################################

Opening the following file:  ./map/ei8246.tsp
#####################
####     End     ####
#####################
### List too long to be displayed ###

Size of population:			4
Size of Tournament Selection:		2
Length of an individual:		8246
Number of iterations: 			50 	Max: 50
Number of iterations without updates: 	0 	Max: 10
Fitness of the Final Individual:    81,379,162,111.00  meters
Elapsed time: 				65.614317 seconds
