"""
This file contains everything related to the Selection Process of an EA
@author: ant0ine
@date: September 2017
@version: v0.1
"""

from random import randint

def rouletteWheelSelection():
    pass

def tournamentSelection(individualsList,
                        tournamentSize=5,
                        typeOfComparaison='low'
                       ):
    """
    Select a parent using the tournamanent selection.
    Inputs: 
        individualsList: Python List - List of Individual()
        
        tournamentSize: int - Size of the tournament to adjust the pressure
        
        typeOfComparaison: str - 'high' | 'low' - Give the direction for the 
            best individuals

    Returns:
        bestIndvdlIndex: int - Index of the selected individual in the list
    """

    # Initialization: Pick a randil Individual
    bestIndvdlIndex = randint(0, len(individualsList)-1)
    bestIndvdl = individualsList[bestIndvdlIndex]

    # Tournament
    for k in range(tournamentSize):
        indvdlIndex = randint(0, len(individualsList)-1)
        if individualsList[indvdlIndex].compareFitness(bestIndvdl,
                                                       typeOfComparaison
                                                      ):

            bestIndvdl = individualsList[indvdlIndex]
            bestIndvdlIndex = indvdlIndex 

    return (bestIndvdlIndex)

def rankBased(power):
    pass

def rankBasedLowBias():
    return rankBased(0.5)

def rankBasedHighBiased():
    return rankBased(2)

    

