#! /usr/bin/env python3
# coding: utf-8

class Individual():
    def __init__(self, data, howToMutate, howToEvaluateFitness):
        self.content = data
        self.mutationMethod = howToMutate
        self.evaluateMethod = howToEvaluateFitness

    def evaluateFitness(self):
        self.fitness = self.evaluateMethod(self.content)
        return self.fitness

    def mutate(self):
        self.content = self.mutationMethod(self.content)
        self.evaluateFitness()

    def clone(self):
        """
        This Individual will clone itself
        """
        return(Individual(self.content,
                          self.mutationMethod, 
                          self.evaluateMethod
                         )
              )

    def compareFitness(self, otherIndividual, typeOfComparaison):
        """
        Aim:
            Compare two individuals based on their fitness.
        Inputs:
            Two individuals objects
            typeOfComparaison: string - 'high' | 'low' depending on the type of
                evaluation we want
        Return:
            Boolean - if the object on which the method is called has a better 
                fitness than the other object based on the typeOfComparaison 
                wanted.
        """
        if typeOfComparaison == 'high':
            if self.evaluateFitness() > otherIndividual.evaluateFitness():
                return True
            else:
                return False

        elif typeOfComparaison == 'low':
            if self.evaluateFitness() < otherIndividual.evaluateFitness():
                return True
            else:
                return False

    def __str__(self):
        content_str = "The node in this list are:"
        i = 0
        for element in self.content:
            i += 1
            content_str += "\n\t" + str(i) + " - City number "\
                        + str(int(element[0])) \
                        + " located at (" + str(element[1]) + ", " \
                        + str(element[2]) + ")"
        return (content_str)



