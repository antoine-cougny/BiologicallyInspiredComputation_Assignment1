""" Approximation of the great circle distance """ 

from numpy import pi, cos, sin, sqrt, arctan2

def computeDistance(i, j, dataset):
    """ 
    Compute the distance between two points of dataset indexed by the given i & j
    """
    lat_i = pi*dataset[i][0]/180.0
    lat_j = pi*dataset[j][0]/180.0
    lon_i = pi*dataset[i][1]/180.0
    lon_j = pi*dataset[i][1]/180.0

    q1 = cos(lat_j)*sin(lon_i - lon_j)
    q3 = sin((lon_i - lon_j)/2.0)
    q4 = cos((lon_j - lon_i)/2.0)
    q2 = sin(lat_i + lat_j)*q3*q3 - sin(lat_i - lat_j)*q4*q4
    q5 = cos(lat_i + lat_j)*q4*q4 - cos(lat_i - lat_j)*q3*q3

    return (int(6378388.0 * arctan2(sqrt(q1*q1 + q2*q2), q5) + 1.0))


def computeTripCost(citiesList):
    """
    Compute the total distance of a trip.
    NB: We go back to the first point.
    """

    distance = 0
    nbCities = len(citiesList)
    for k in range(nbCities-1):
        distance += computeDistance(k, k+1, citiesList)

    # Because we go back to the first point
    distance += computeDistance(nbCities-1, 0, citiesList)

    return (distance)
