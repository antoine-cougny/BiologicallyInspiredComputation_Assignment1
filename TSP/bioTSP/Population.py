#! /usr/bin/env python3
# coding: utf-8

import numpy as np
import copy
from .Individual import Individual
from .Mutation import *
from .Selection import *
from .tool import parse

class Population():
    def __init__(self, 
                 filepath, 
                 selectionMethod,
                 evaluateFitnessMethod, 
                 mutateMethod, 
                 updateMethod,
                 sizeOfPop=1,
                 sizeOfSelection=5,
                 typeOfComparaison='low',
                 numberOfCities=None, 
                 maxNbIter=300,
                 maxNbIterWithoutChange=100
                ):
        self.sizeOfPop              = sizeOfPop
        self.maxNbIter              = maxNbIter
        self.maxNbIterWithoutChange = maxNbIterWithoutChange
        self.typeOfComparaison      = typeOfComparaison
        self.numberOfCities         = numberOfCities
        self.sizeOfSelection        = sizeOfSelection

        self.individuals     = self.initPop(filepath,
                                            mutateMethod, 
                                            evaluateFitnessMethod
                                           )
        self.selectionMethod = selectionMethod
        self.updateMethod    = updateMethod

    def initialization(self, arrayOfCities, mutateMethod, evaluateFitnessMethod):
        #####################################
        ###         Initialization        ###
        #####################################

        # Take numberOfCities cities randomly
        # citiesList = CustomList()
        citiesListReg = [0]*self.numberOfCities
        for i in range(self.numberOfCities):
            indexRnd = int(np.random.random()*len(arrayOfCities))
            # We remove the city from the list and we keep it
            city = arrayOfCities.pop(indexRnd) 
            # We s(tore it
            # citiesList.insertFirst(city)
            citiesListReg[i] = city         # Use of Python list for backup

        # We create the Individual object
        individual = Individual(citiesListReg, mutateMethod, evaluateFitnessMethod)

        return individual

    def initPop(self, filepath, mutateMethod, evaluateFitnessMethod):
        # Openfile & parse it
        arrayOfCities = parse(filepath)

        # Check if we take all the cities or not
        if self.numberOfCities is None:
            self.numberOfCities = len(arrayOfCities)

        listOfIndividual = [0]*self.sizeOfPop
        for k in range(self.sizeOfPop):
            listOfIndividual[k] = self.initialization(copy.copy(arrayOfCities),
                                                      mutateMethod,
                                                      evaluateFitnessMethod
                                                     )

        return listOfIndividual

    def selectIndividual(self):
        """
        Use the provided selection method given at the creation of the Population
        object to select one individual among the population.
        """
        return self.selectionMethod(self.individuals,
                                    self.sizeOfSelection, 
                                    self.typeOfComparaison
                                   )

    def updatePopulation(self, newIndividual):
        """
        Update the population by using the function given at the creation of the
        Population object.
        """

        self.individuals = self.updateMethod(self.individuals,
                                             newIndividual,
                                             self.typeOfComparaison 
                                            )

    def runEvolution(self):
        #####################################
        ###     Evoluation in Progress    ###
        #####################################

        # This ensure that we leave the loop at some point. /!\ We may end up 
        # on a local min/max
        isProgessing = self.maxNbIterWithoutChange   

        nbIter = 0
        while(isProgessing and nbIter<self.maxNbIter):
            # We know how long it took
            nbIter += 1

            # Selection
            individualIdx = self.selectIndividual()
            individual = self.individuals[individualIdx]

            # Clone
            newIndividual = individual.clone()

            # Random Mutation
            newIndividual.mutate()
            
            # Test if we update the population
            # if self.updatePopulation(newIndividual):
            if self.updateMethod(self.individuals,
                                 newIndividual,
                                 self.typeOfComparaison
                                ):
                # If we updated it, reset the counter
                isProgessing = self.maxNbIterWithoutChange
            else:
                # We have not updated the population. Maybe we are at an extremum
                isProgessing -= 1
        
        bestIndividualIdx = self.selectionMethod(self.individuals,
                                                 len(self.individuals), 
                                                 self.typeOfComparaison
                                                )

        return self.individuals[bestIndividualIdx], nbIter, isProgessing

if __name__ == '__main__':
    pass

