#! /usr/bin/env python3
# coding:utf-8

"""
The representation of the itinary used for the TSP algorithm will be a chained 
list.
NB: This class is not used anymore in the EA
"""

class Node():
    """
    Self referenced class used to build chained lists.
    Instance variables:
        next: a reference to the next node
        prev: a reference to the previous node (double chained list)
        content: what the node is about
    """
    def __init__(self, data):
        # self.id = id_num
        # self.lat = lat
        # self.lng = lng
        self.content = data
        self.next = None
        self.prev = None

    def __str__(self):
        """
        Overwriting the string description of the instance
        """
        return ("\n\tThis node is the city number %i located at (%f, %f)"
                %(self.content[0], self.content[1], self.content[2]))

class CustomList():
    """
    Class implementing a double chained list
    """
    def __init__(self, data=None):
        """
        CustomList constructor, calling it will return an empty List with
        a ghost Node as its first element.
        Parameters: None
        """
        ghostNode = Node(data)
        ghostNode.next = ghostNode
        ghostNode.prev = ghostNode
        self.first = ghostNode
        self.last = ghostNode


    def insertFirst2(self, data):
        """
        Create a new Node, set data as its content and put it as the beginning
        of the list.
        """
        new_node = Node(data)
        
        # We save the first node
        tmp_first = self.first
        tmp_last = self.first.prev # self.last

        # Insert at first position
        self.first.next = new_node

        # Link the rest of the chain
        new_node.next = tmp_first
        new_node.prev = tmp_last

        # Update the previous node
        tmp_first.prev = new_node
        tmp_last.next = new_node
    
    def insertFirst(self, data):
        if self.first.content is None:
            self.first.content = data
        else:
            new_node = Node(data)
            
            first_node = self.first
            new_node.next = first_node
            new_node.prev = first_node.prev
            new_node.prev.next = new_node
            first_node.prev = new_node

            ## Simple chained list
            # last_node = self.goToLastNode()
            # last_node.next = new_node
            self.first = new_node
    
    def goToLastNode(self):
        """
        Returns the last node of the chained list
        """
        first_node = self.first
        n = first_node.next

        while (n.next != first_node):
            n = n.next

        return (n)

    def insertLast(self, data):
        """
        Create a new Node, set data as its content and put it as the end of the
        list.
        """
        new_node = Node(data)

        # self.last = new_node
        last_node = self.first.prev # self.last
        last_node.next = new_node
        new_node.prev = last_node
        # new_node.prev.next = new_node
        new_node.next = self.first
        self.first.prev = new_node

    def giveSize(self):
        """
        """
        return (self.__len__())

    def __len__(self):
        first_node = self.first
        size = 1
        node = first_node

        while (node.next != first_node):
            node = node.next
            size += 1

        return (size)

    def insert(self, data, idx):
        """
        Create a new Node, set data as its content and put it into the list
        such as its position will be idx. If idx is greater than the list
        length the node will be inserted at the end of the list.
        Parameters:
            data: the content of the new node
            idx: integer, the position of the node in the list
                 can be positive or negative
        Return value:
            False if idx == 0, True otherwise
        """

        # size = self.giveSize()
        # if idx < 0:
        #     print("Can not insert at negative index")
        #     return False
        new_node = Node(data)
        counter = 1

        if idx == 0:
            print("Index not valid")
            return False
        
        if idx > 0:
            n = self.first
            first_node = n
            while n.next != first_node and counter != idx-1:
                n = n.next
                counter += 1
            # Insert the node
            new_node.next = n.next
            new_node.prev = n
            # Update the node next to the new node
            n.next = new_node
            new_node.next.prev = new_node

        # Does not work yet
        # TODO: fix that
        #Quick fix
        if idx<0:
            lrg = self.giveSize()
            idx = lrg + idx
            self.insert(data, idx)

#         if idx < 0:
#             n = self.first.prev # self.last
#             last_node = n
#             while n.prev != last_node and counter != idx+1:
#                 n = n.prev
#                 counter -= 1
#             # Insert the node
#             new_node.prev = n.prev
#             new_node.next = n

#             n.prev = new_node
            new_node.prev.next = new_node

        return True
    ## TODO check 

    def insertPositive(self, data, idx):
        pass

    def insertNegative(self, data, idx):
        pass

    def search(self, data):
        """
        Search into the list for the first node whose content equals data. The
        meaning of "equals" is what the node's content implements as __eq__
        operator (i.e ==)

        Parameters:
            data: the node content to search for
        Return value:
            The node index on success or None if the node is not found.
        """
        cur_node = self.first
        while cur_node.next != None and not(cur_node.next.content == data) :
            cur_node = cur_node.next
        return cur_node.next

    def delete(self, data):
        """
        Search into the list for the first node whose content equals data and
        then remove it from the list. The meaning of "equals" is what the
        node's content implements as __eq__ operator (i.e ==)

        Parameters:
            data: the node content to remove
        Return value:
            True success or False if no node has this content.
        """
        cur_node = self.first
        while cur_node.next != None and not(cur_node.next.content == data) :
            cur_node = cur_node.next
        # At this point, cur_node.next is the node we want to delete/skip
        cur_node.next = cur_node.next.next 
        cur_node.next.prev = cur_node
        # Now we cannot find the given data in the list

    def goToIndexNode(self, idx):
        """
        Return:
            Node at the idx position
        """
        counter = 1
        if idx == 0:
            print("Error of Index!")
            return (Node(None))

        if idx > 0:
            n = self.first
            while (counter < idx):
                counter += 1
                n = n.next

        elif idx < 0:
            # TODO: check that. probably pb in insertion with node.prev
            n = self.first.prev
            while (counter > idx):
                counter -= 1
                n = n.prev
        return n

    def __str__(self):
        first_node = self.first
        n = first_node.next
        content_str = "The node in this list are:"
        i = 1
        content_str += str(first_node) + ' - ' + str(i)
        while n != first_node and n is not None:
            i += 1
            content_str +=  str(n) + ' - ' + str(i)
            n = n.next
        return (content_str)


if __name__ == '__main__':
    pt1 = [1, 34.4, 45.4]
    pt2 = [2, 34.34, 45.3]

    cList = CustomList(pt2)
    print(cList)
    cList.insertFirst(pt1)
    cList.insertLast([3, 14, 14])
    print(cList)
    cList.insertFirst([5, 23, 54])
    print(cList)
    cList.insert([7, 12, 34], 2)

    print(cList)
