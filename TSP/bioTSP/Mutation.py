#! /usr/bin/env python3
# coding: utf-8

"""
This files contains everything related to the mutation of the new child
"""

from numpy import random
from copy import deepcopy

def processMutation():
    pass

def karyEncodingsMutation():
    pass

def karyEncodingsCrossover():
    pass

def uniformCrossover():
    pass

def orderBasedEncodingMutation():
    pass

def kInversionMutation():
    pass

def kGeneOrderBasedCrossover():
    pass

def kGenePositionBasedCrossover():
    pass

def singlePointCrossover(parent1, parent2):
    pass

def blendCrossover(parent1, parent2):
    pass

def swapTwoElements(dataset):
    """
    Aim:
        Select two elements from the list and swap them
    Input:
        Python list of cities
    Return:
        A new list of cities
    """
    nbCities = len(dataset)
    i = int(nbCities*random.random())
    j = int(nbCities*random.random())
    newDataset = deepcopy(dataset)
    newDataset[i], newDataset[j] = newDataset[j], newDataset[i]

    return (newDataset)
