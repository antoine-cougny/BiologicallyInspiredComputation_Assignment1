"""
This files contains everything related to the update of the population.
"""

from .Selection import tournamentSelection

def updatePopulation():
    """
    Update the population according to the selected algo at the 
    initialization of the EA
    """
    pass

def replaceRandomly():
    pass

def replaceWorst(listOfIndividuals, newIndividual, typeOfComparaison):
    """
    Replace the worst individual of the population by the new one
    It uses the tournamentSelection function but on all of the population.

    Returns:
        changeHappened: True | False - If an update has been made
    """
    sizeOfPop = len(listOfIndividuals)
 
    # Inversion of the search to look for the worst instead of the best
    if typeOfComparaison == 'low':
        search = 'high'
    if typeOfComparaison == 'high':
        search = 'low'   

    # Get the worst individual
    indexWorstIndvdl = tournamentSelection(listOfIndividuals,
                                           sizeOfPop,
                                           search 
                                          )
    changeHappened = False

    # Fitness Evaluation
    # Keep it ?
    if newIndividual.compareFitness(listOfIndividuals[indexWorstIndvdl],
                                    typeOfComparaison
                                   ):
        # Update it
        listOfIndividuals[indexWorstIndvdl] = newIndividual
        changeHappened = True

    return changeHappened

def replaceParents():
    pass


