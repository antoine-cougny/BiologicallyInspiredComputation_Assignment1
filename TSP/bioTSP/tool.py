"""
This file contains different functions used by the algo
"""

from numpy import array

def parse(path):
    """
    This function read the file given according to the provided esp file
    It returns a python list 
    """

    # Open file, read-only
    file = open(path, 'r')
    data = file.read()
    # We don't need the file access anymore
    file.close()
    # Split data per lines
    data = data.split("\n")

    # Parse the data into an array
    start = 7   # The 7 first lines are metadata
    data_array = [0]*(len(data) - start - 2)    # We want to get rid of the 'EOF'
                                                # & of one empty line at the end
    for i in range(start, len(data) - 2):
        data_array[i - start] = data[i].split(' ')
        while True:
            try:
                data[i].remove('')
            except:
                break
    # print(data_array)
    data_array = [ [float(elt) for elt in line] for line in data_array]
    return (data_array)

if __name__ == "__main__":
    """ Unit test with the file of the coursework 
    """
    a = parse('./../map/ei8246.tsp')
    print(a)
