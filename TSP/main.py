#! /usr/bin/env python3
# coding: utf-8

import sys
import time

#sys.path.append('./src/')
#from ChainedList import CustomList 
#from TSP_Cost import computeTripCost
#from Mutation import swapTwoElements
#from Population import Population
#from Update import *
#from Selection import *
from bioTSP import *

def main(filepath, 
         selectionMethod, 
         evaluateFitnessMethod,
         mutateMethod,
         updateMethod,
         sizeOfPop=10,
         sizeOfSelection=5,
         typeOfComparaison='low',
         numberOfCities=None, 
         maxNbIter=300,
         maxNbIterWithoutChange=100
        ):
    
    print('\n\n##############################################################')
    print('\nOpening the following file: ', filepath)

    # Create Population() object
    population = Population(filepath,
                            selectionMethod,
                            evaluateFitnessMethod,
                            mutateMethod,
                            updateMethod,
                            sizeOfPop,
                            sizeOfSelection,
                            typeOfComparaison,
                            numberOfCities,
                            maxNbIter,
                            maxNbIterWithoutChange
                           )

    # Print before EA
    # print("\nPopulation of %i individuals"%(len(population.individuals)))

    # Time before
    time_init = time.time()
    
    # Run Evoluation & Get the best individual of the population
    bestIndvdl, nbIter, nbWithoutUpdate = population.runEvolution()

    # Time after
    time_end = time.time()

    # Display result
    print("#####################")
    print("####     End     ####")
    print("#####################")

    if (len(bestIndvdl.content) > 100):
        print("### List too long to be displayed ###")
    else:
        print("\n",bestIndvdl)

    print("\nSize of population:\t\t\t%i"%(sizeOfPop))
    print("Size of Tournament Selection:\t\t%i"%(sizeOfSelection))
    print("Length of an individual:\t\t%i"%(len(bestIndvdl.content)))
    print("Number of iterations: \t\t\t%i \tMax: %i"%(nbIter, maxNbIter))
    print("Number of iterations without updates: \t%i \tMax: %i"
          %(maxNbIterWithoutChange - nbWithoutUpdate, maxNbIterWithoutChange)
         )
    print('Fitness of the Final Individual:', 
          '{:20,.2f}'.format(bestIndvdl.evaluateFitness()), 
          ' meters')
    print("Elapsed time: \t\t\t\t%f seconds"%(time_end - time_init))

def hillclimbing(numberOfGenerations,
                 filepath, 
                 selectionMethod, 
                 evaluateFitnessMethod,
                 mutateMethod,
                 updateMethod,
                 sizeOfPop=1,
                 sizeOfSelection=1,
                 typeOfComparaison='low',
                 numberOfCities=None, 
                 maxNbIter=300,
                 maxNbIterWithoutChange=100
                ):

    print('\n\n##############################################################')
    print('\nOpening the following file: ', filepath)
    print('Size of the population:\t\t', sizeOfPop)
    print('Size of the selection:\t\t', sizeOfSelection)

    # Time before
    time_init = time.time()
    
    # Keep the individuals
    listOfResults = numberOfGenerations*[0]

    # Run Evoluation & Get the best individual of the population
    # Create Population() object
    for k in range(numberOfGenerations):
        population = Population(filepath,
                                selectionMethod,
                                evaluateFitnessMethod,
                                mutateMethod,
                                updateMethod,
                                sizeOfPop,
                                sizeOfSelection,
                                typeOfComparaison,
                                numberOfCities,
                                maxNbIter,
                                maxNbIterWithoutChange
                               )
        bestIndvdl, nbIter, nbWithoutUpdate = population.runEvolution()
        listOfResults[k] = (bestIndvdl, nbIter, nbWithoutUpdate)

    # Time after
    time_end = time.time()
    
    print("Number of generations: \t\t\t%i\n"%(numberOfGenerations))
    for element in listOfResults:
        nbIter = element[1]
        nbWithoutUpdate= element[2]

        print("Number of iterations: \t\t\t%i \tMax: %i"%(nbIter, maxNbIter))
        print("Number of iterations without updates: \t%i \tMax: %i"
              %(maxNbIterWithoutChange - nbWithoutUpdate, maxNbIterWithoutChange)
             )

        print('Fitness of the Final Individual:', 
              '{:20,.2f}'.format(element[0].evaluateFitness()), 
              ' meters\n')
    print("Elapsed time: \t\t\t\t%f seconds"%(time_end - time_init))


if __name__ == '__main__':
    if len(sys.argv) == 1:
        filepath            = './map/ei8246.tsp'
        method              = 'hillclimbing'
        numberOfGenerations = 5
    elif len(sys.argv) == 2:
        filepath            = sys.argv[1]
        method              = 'hillclimbing'
        numberOfGenerations = 5
    elif len(sys.argv) == 3:
        filepath            = sys.argv[1]
        method              = sys.argv[2]
        numberOfGenerations = 5
    else:
        filepath            = sys.argv[1]
        method              = sys.argv[2]
        numberOfGenerations = int(sys.argv[3])

    # How to use ?
    # Change the parameters in the following function call
    
    if method == 'main':
        main(filepath,
             selectionMethod        = tournamentSelection,
             evaluateFitnessMethod  = computeTripCost,
             mutateMethod           = swapTwoElements,
             updateMethod           = replaceWorst,
             sizeOfPop              = 12,
             sizeOfSelection        = 6,
             typeOfComparaison      = 'low',
             numberOfCities         = None,
             maxNbIter              = 1000,
             maxNbIterWithoutChange = 100
            )
    elif method == 'hillclimbing':
        hillclimbing(numberOfGenerations,
                     filepath,
                     selectionMethod        = tournamentSelection,
                     evaluateFitnessMethod  = computeTripCost,
                     mutateMethod           = swapTwoElements,
                     updateMethod           = replaceWorst,
                     sizeOfPop              = 1,
                     sizeOfSelection        = 1,
                     typeOfComparaison      = 'low',
                     numberOfCities         = None,
                     maxNbIter              = 1000,
                     maxNbIterWithoutChange = 100
                    )
 
